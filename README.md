# README #

back-end repository that connect with https://bitbucket.org/Tiendas/tienda-font (Angular).

### .env file ####
Set env file locally, otherwise it will not be possible to create users.

### What is this repository for? ###

* repositorio del backend Back-end
* 1.0


### How do I get set up? ###

* bundle install
* rake db:create
* rake db:migrate
* rails s